FROM busybox:latest
MAINTAINER Rob Parker <rob@parob.com>

COPY index.html /www/index.html

EXPOSE 8080
HEALTHCHECK CMD nc -z localhost 8080

CMD trap "exit 0;" TERM INT; httpd -p 8080 -h /www -f & wait